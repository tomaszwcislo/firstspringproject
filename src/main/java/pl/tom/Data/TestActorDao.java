package pl.tom.Data;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import pl.tom.Entity.Actor;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@Repository
@Qualifier("Test")
public class TestActorDao implements ActorDao {

    private static Map<Integer, Actor> actors;

    static {

        actors = new HashMap<Integer, Actor>();

            actors.put(1, new Actor(1, "Arnold", "Schwarzenegger"));
            actors.put(2, new Actor(2, "Sylwester", "Stallone"));
            actors.put(3, new Actor(3, "Bruce", "Willis"));
            actors.put(4, new Actor(4, "Tom", "Hanks"));

    }
    @Override
    public Collection<Actor> getAllActors() {
        return actors.values();
    }

    @Override
    public Actor getActorById(int id) {
        return this.actors.get(id);
    }

    @Override
    public void deleteActorById(int id) {
        this.actors.remove(id);
    }

    @Override
    public void updateActor(Actor actor) {
        Actor a = actors.get(actor.getId());
        a.setFirstname(actor.getFirstname());
        a.setLastname(actor.getLastname());
        actors.put(actor.getId(), actor);
    }

    @Override
    public void createActor(Actor actor) {
        this.actors.put(actor.getId(), actor);
    }
}
