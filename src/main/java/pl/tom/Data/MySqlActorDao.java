package pl.tom.Data;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import pl.tom.Entity.Actor;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

@Repository
@Qualifier("mysql")
public class MySqlActorDao implements ActorDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public static class ActorRowMapper implements RowMapper<Actor> {

        @Override
        public Actor mapRow(ResultSet resultSet, int i) throws SQLException {
            Actor actor = new Actor();
            actor.setId(resultSet.getInt("id"));
            actor.setFirstname(resultSet.getString("firstname"));
            actor.setLastname(resultSet.getNString("lastname"));
            return actor;
        }
    }
    @Override
    public Collection<Actor> getAllActors() {
        final String sql = "SELECT * FROM Actor";
        List<Actor> actors = jdbcTemplate.query(sql, new ActorRowMapper());
        return actors;
    }

    @Override
    public Actor getActorById(int id) {
        final String sql = "SELECT * FROM Actor WHERE id = ?";
        Actor actor = jdbcTemplate.queryForObject(sql, new ActorRowMapper(), id);
        return actor;
    }

    @Override
    public void deleteActorById(int id) {
        final String sql = "DELETE FROM Actor WHERE id = ?";
        jdbcTemplate.update(sql, id);
    }

    @Override
    public void updateActor(Actor actor) {
        final String sql = "UPDATE Actor SET firstname = ?, lastname = ? WHERE id = ?";
        final int id = actor.getId();
        final String firstname = actor.getFirstname();
        final String lastname = actor.getLastname();
        jdbcTemplate.update(sql, new Object[] {firstname, lastname, id});
    }

    @Override
    public void createActor(Actor actor) {
        final String sql = "INSERT INTO Actor (firstname, lastname) VALUES (?, ?)";
        final String firstname = actor.getFirstname();
        final String lastname = actor.getLastname();
        jdbcTemplate.update(sql, new Object[] {firstname, lastname});
    }
}
