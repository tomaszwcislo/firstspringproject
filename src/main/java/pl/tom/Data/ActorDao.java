package pl.tom.Data;

import pl.tom.Entity.Actor;

import java.util.Collection;

public interface ActorDao {
    Collection<Actor> getAllActors();

    Actor getActorById(int id);

    void deleteActorById(int id);

    void updateActor(Actor actor);

    void createActor(Actor actor);
}
