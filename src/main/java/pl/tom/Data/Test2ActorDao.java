package pl.tom.Data;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import pl.tom.Entity.Actor;

import java.util.ArrayList;
import java.util.Collection;

@Repository
@Qualifier("Test2")
public class Test2ActorDao implements ActorDao {

    @Override
    public Collection<Actor> getAllActors() {
        return new ArrayList<Actor>(){
            {
                add(new Actor(1, "Bruce", "Lee"));
            }
        };
    }

    @Override
    public Actor getActorById(int id) {
        return null;
    }

    @Override
    public void deleteActorById(int id) {

    }

    @Override
    public void updateActor(Actor actor) {

    }

    @Override
    public void createActor(Actor actor) {

    }
}
