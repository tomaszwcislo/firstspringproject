package pl.tom.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import pl.tom.Entity.Actor;
import pl.tom.Service.ActorService;
import java.util.Collection;

import static org.springframework.web.bind.annotation.RequestMethod.*;

@RestController
@RequestMapping("/actors")
public class ActorController {

    @Autowired
    private ActorService actorService;

    @RequestMapping(method = GET)
    public Collection<Actor> getAllActors() {

        return actorService.getAllActors();
    }
    @RequestMapping(value = "/{id}", method = GET)
    public Actor getActorById(@PathVariable("id") int id) {

        return actorService.getActorById(id);
    }
    @RequestMapping(value = "/{id}", method = DELETE)
    public void deleteActorById(@PathVariable("id") int id) {

        actorService.deleteActorById(id);
    }
    @RequestMapping(method = PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    public void updateActor(@RequestBody Actor actor) {

        actorService.updateActor(actor);
    }
    @RequestMapping(method = POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public void createActor(@RequestBody Actor actor) {

        actorService.createActor(actor);
    }
}
