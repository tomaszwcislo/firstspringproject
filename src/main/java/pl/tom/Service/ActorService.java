package pl.tom.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import pl.tom.Entity.Actor;
import java.util.Collection;

@Service
public class ActorService {

    @Autowired
    @Qualifier("mysql")
    private pl.tom.Data.ActorDao ActorDao;

    public Collection<Actor> getAllActors() {
        return this.ActorDao.getAllActors();
    }

    public Actor getActorById(int id) {
        return this.ActorDao.getActorById(id);
    }

    public void deleteActorById(int id) {
        this.ActorDao.deleteActorById(id);
    }
    public void updateActor(Actor actor) {
        this.ActorDao.updateActor(actor);
    }

    public void createActor(Actor actor) {
        this.ActorDao.createActor(actor);
    }
}
